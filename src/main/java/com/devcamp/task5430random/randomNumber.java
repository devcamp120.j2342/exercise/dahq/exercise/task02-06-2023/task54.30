package com.devcamp.task5430random;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class randomNumber {
    @CrossOrigin
    @GetMapping("/double-random")
    public String randomDouble() {
        double randoom = 0;
        for (int i = 0; i < 100; i++) {
            randoom = Math.random() * 100;
        }
        return "random double 1 to 100: " + randoom;
    }

    public String randomInt() {
        int randoom = 1 + (int) (Math.random() * ((10 - 1) + 1));
        return "random int 1 to 100: " + randoom;
    }

}
