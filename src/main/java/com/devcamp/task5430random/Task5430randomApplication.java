package com.devcamp.task5430random;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task5430randomApplication {

	public static void main(String[] args) {
		SpringApplication.run(Task5430randomApplication.class, args);
		randomNumber randomNumber = new randomNumber();
		System.out.println(randomNumber.randomDouble());
		System.out.println(randomNumber.randomInt());

	}

}
